<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php;

use BadPixxel\Robo\Php\Robo\Plugin\Tasks\Grumphp;
use Robo\Collection\CollectionBuilder;

/**
 * Trait for Using Grumphp Tasks
 */
trait GrumphpTasksTrait
{
    /**
     * Execute a Single Grumphp Task
     *
     * @return CollectionBuilder|Grumphp\SingleTask
     */
    protected function taskGrumphpTask()
    {
        return $this->task(Grumphp\SingleTask::class);
    }

    /**
     * Execute Multiple Grumphp Tasks
     *
     * @return CollectionBuilder|Grumphp\MultipleTasks
     */
    protected function taskGrumphpTasks()
    {
        return $this->task(Grumphp\MultipleTasks::class);
    }
}
