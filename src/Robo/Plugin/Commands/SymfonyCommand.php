<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php\Robo\Plugin\Commands;

use Robo\Symfony\ConsoleIO;
use Robo\Tasks;

class SymfonyCommand extends Tasks
{
    /**
     * @command add:symfony
     *
     * @description Install Symfony CLI
     */
    public function installSymfony(ConsoleIO $consoleIo): int
    {
        //====================================================================//
        // Create Temporary Path
        $tmpPath = $this->_tmpDir();
        //====================================================================//
        // Download Composer Installer
        $this->_copy("https://get.symfony.com/cli/installer", $tmpPath."/symfony-setup.sh");
        //====================================================================//
        // Execute Install
        $install = $this->_exec("bash ".$tmpPath.'/symfony-setup.sh');
        if (!$install->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Move CLI Binary
        $move = $this->_copy("/root/.symfony5/bin/symfony", "/usr/local/bin/symfony");
        if (!$move->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Show Installed Version
        $this->_exec('symfony -v');
        //====================================================================//
        // Notify User
        $consoleIo->success("Symfony CLI Now Installed");

        return 0;
    }
}
