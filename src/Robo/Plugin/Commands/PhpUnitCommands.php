<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php\Robo\Plugin\Commands;

use Robo\Symfony\ConsoleIO;
use Robo\Tasks;
use Symfony\Component\Console\Command\Command;

/**
 * PHP Apps Basic Commands
 */
class PhpUnitCommands extends Tasks
{
    /**
     * @command add:phpunit
     *
     * @description Install Phpunit
     */
    public function installComposer(ConsoleIO $consoleIo, string $version = "9"): int
    {
        //====================================================================//
        // Download Phpunit Executable
        $this->_copy(
            sprintf("https://phar.phpunit.de/phpunit-%s.phar", $version),
            "/usr/local/bin/phpunit"
        );
        //====================================================================//
        // Make Executable
        if (!$this->_chmod("/usr/local/bin/phpunit", 10)->wasSuccessful()) {
            return Command::FAILURE;
        }
        //====================================================================//
        // Show Installed Version
        $this->_exec('phpunit --version');
        //====================================================================//
        // Notify User
        $consoleIo->success(
            sprintf("Phpunit %s Now Installed", $version)
        );

        return Command::SUCCESS;
    }

    /**
     * Force Using Composer Version
     *
     * @param ConsoleIO   $consoleIo Console Outputs
     * @param null|string $version   Requested Version (1, 2)
     *
     * @return bool
     */
    protected function forceComposerVersion(ConsoleIO $consoleIo, string $version = null): bool
    {
        //====================================================================//
        // Safety Check
        if ($version && !in_array($version, array("1", "2"), true)) {
            $consoleIo->warning('Wrong Composer version. Should be null|1|2');
        }
        //====================================================================//
        // Safety Check
        if (in_array($version, array("1", "2"), true)) {
            return $this->_exec(sprintf('composer self-update --%s;', $version))->wasSuccessful();
        }

        return true;
    }
}
