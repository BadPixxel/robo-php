<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php\Robo\Plugin\Commands;

use Robo\Symfony\ConsoleIO;
use Robo\Tasks;

/**
 * PHP Apps Basic Commands
 */
class ComposerCommands extends Tasks
{
    /**
     * @command add:composer
     *
     * @description Install Composer
     *
     * @param null|string $version
     *
     * @return int
     */
    public function installComposer(ConsoleIO $consoleIo, string $version = null): int
    {
        //====================================================================//
        // Create Temporary Path
        $tmpPath = $this->_tmpDir();
        //====================================================================//
        // Download Composer Installer
        $this->_copy("https://getcomposer.org/installer", $tmpPath."/composer-setup.php");
        //====================================================================//
        // Execute Install
        $install = $this->_exec('php '.$tmpPath.'/composer-setup.php --install-dir=/usr/local/bin --filename=composer');
        if (!$install->wasSuccessful()) {
            return 1;
        }
        //====================================================================//
        // Force Install of a Dedicated Version
        if (!$this->forceComposerVersion($consoleIo, $version)) {
            return 1;
        }
        //====================================================================//
        // Show Installed Version
        $this->_exec('composer --version');
        //====================================================================//
        // Notify User
        $consoleIo->success(
            sprintf("Composer %s Now Installed", $version ?? 'Latest')
        );

        return 0;
    }

    /**
     * Force Using Composer Version
     *
     * @param ConsoleIO   $consoleIo Console Outputs
     * @param null|string $version   Requested Version (1, 2)
     *
     * @return bool
     */
    protected function forceComposerVersion(ConsoleIO $consoleIo, string $version = null): bool
    {
        //====================================================================//
        // Safety Check
        if ($version && !in_array($version, array("1", "2"), true)) {
            $consoleIo->warning('Wrong Composer version. Should be null|1|2');
        }
        //====================================================================//
        // Safety Check
        if (in_array($version, array("1", "2"), true)) {
            return $this->_exec(sprintf('composer self-update --%s;', $version))->wasSuccessful();
        }

        return true;
    }
}
