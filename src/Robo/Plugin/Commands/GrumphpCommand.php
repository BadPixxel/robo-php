<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php\Robo\Plugin\Commands;

use BadPixxel\Robo\Extras\Console\ResultsBlock;
use BadPixxel\Robo\Php\GrumphpTasksTrait;
use BadPixxel\Robo\Php\Robo\Plugin\Tasks\Grumphp\MultipleTasks;
use BadPixxel\Robo\Php\Robo\Plugin\Tasks\Grumphp\SingleTask;
use Robo\Symfony\ConsoleIO;
use Robo\Tasks;

class GrumphpCommand extends Tasks
{
    use GrumphpTasksTrait;

    /** @var string[] */
    const QUALITY_TASKS = array(
        "composer" => "Composer => Check Dependencies Status",
        "phplint" => "PhpLint => Verify PHP Code Syntax vs PHP Version",
        "jsonlint" => "JsonLint => Verify Json Files Syntax",
        "xmllint" => "XmlLint => Verify Xml Files Syntax",
        "yamllint" => "YamlLint => Verify Yaml Files Syntax",
        "twigcslint" => "TwigCsLint => Verify Twig Files Syntax",
        "phpcpd" => "PhpCpd => Search for Duplicate pieces of Code",
        "phpcs" => "Php CS => Verify Code Styling",
        "phpmd" => "PHP MD => Mess Detector",
        "phpcsfixer" => "PHP CS Fixer => Automatic CS Fixing"
    );

    /** @var string[] */
    const PHPSTAN_TASKS = array(
        "phpstan" => "Phpstan => PHP Code Static Analyze",
    );

    /**
     * @command grumphp:one
     *
     * @description Execute a Single GrumpPhp Task
     *
     * @param string      $code Grumphp Task Name
     * @param null|string $path Force Path Prefix
     *
     * @return int
     */
    public function one(ConsoleIO $consoleIo, string $code, string $path = null): int
    {
        /** @var SingleTask $singleTask */
        $singleTask = $this->taskGrumphpTask();

        //====================================================================//
        // Execute Grumphp Tasks
        $result = $singleTask
            ->location($path)
            ->code($code)
            ->run()
        ;

        $result->wasSuccessful()
            ? $consoleIo->success("Grumphp Task Succeeded")
            : $consoleIo->error("Grumphp Task Fail")
        ;

        return (int)!$result->wasSuccessful();
    }

    /**
     * @command grumphp:quality
     *
     * @description Execute GrumpPhp Quality Standards Verifications
     *
     * @param null|string $path Force Path Prefix
     *
     * @return int
     */
    public function quality(ConsoleIO $consoleIo, string $path = null): int
    {
        return $this->multiple(
            $consoleIo,
            self::QUALITY_TASKS,
            "Quality Standards Suite",
            $path
        );
    }

    /**
     * @command grumphp:stan
     *
     * @description Execute GrumpPhp PhpStan Verifications
     *
     * @param null|string $path Force Path Prefix
     *
     * @return int
     */
    public function stan(ConsoleIO $consoleIo, string $path = null): int
    {
        return $this->multiple(
            $consoleIo,
            self::PHPSTAN_TASKS,
            "PhpStan Suite",
            $path
        );
    }

    /**
     * @command grumphp:full
     *
     * @description Execute GrumpPhp Full Verifications
     *
     * @param null|string $path Force Path Prefix
     *
     * @return int
     */
    public function full(ConsoleIO $consoleIo, string $path = null): int
    {
        return $this->multiple(
            $consoleIo,
            array_merge(self::QUALITY_TASKS, self::PHPSTAN_TASKS),
            "Full Test Suite",
            $path
        );
    }

    /**
     * Execute a Set of Grumphp Tasks
     *
     * @param ConsoleIO   $consoleIo
     * @param array       $tasks
     * @param string      $title
     * @param null|string $path
     *
     * @return int
     */
    private function multiple(ConsoleIO $consoleIo, array $tasks, string $title, string $path = null): int
    {
        /** @var MultipleTasks $multiTask */
        $multiTask = $this->taskGrumphpTasks();
        //====================================================================//
        // Execute Grumphp Tasks
        $result = $multiTask
            ->location($path)
            ->progress(empty(getenv("CI")))
            ->list($tasks)
            ->run()
        ;
        //====================================================================//
        // Render Results
        $bloc = new ResultsBlock($consoleIo);
        $bloc->results($result->getData(), "Grumphp - ".$title);

        return (int) !$result->wasSuccessful();
    }
}
