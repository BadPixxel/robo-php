<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php\Robo\Plugin\Tasks\Grumphp;

use Robo\Result;

/**
 * Execute a Single Grumphp Task
 */
class SingleTask extends AbstractGrumphpTask
{
    /**
     * Grumphp Task Name
     *
     * @var null|string
     */
    private ?string $code = null;

    /**
     * Set Grumphp Task Code
     *
     * @param string $code
     *
     * @return $this
     */
    public function code(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        if (!$this->code) {
            return Result::error($this, "No Grumphp Task Code Given");
        }

        return $this->runGrumpTask($this->code)
            ? Result::success($this, sprintf("Grumphp Task %s Passed", $this->code))
            : Result::error($this, sprintf("Grumphp Task %s Fail", $this->code))
        ;
    }
}
