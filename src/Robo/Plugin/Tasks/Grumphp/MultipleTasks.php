<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php\Robo\Plugin\Tasks\Grumphp;

use Robo\Result;

/**
 * Execute Multiple Grumphp Tasks
 */

class MultipleTasks extends AbstractGrumphpTask
{
    /**
     * Grumphp Tasks List [code => name]
     *
     * @var array<string, string>
     */
    private array $list = array();

    /**
     * Set Grumphp Tasks List
     *
     * @param array<string, string> $list
     *
     * @return $this
     */
    public function list(array $list): self
    {
        $this->list = $list;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function run(): Result
    {
        //====================================================================//
        // Execute Grumphp Tasks
        $results = $this->runGrumpTasks($this->list);
        //====================================================================//
        // Prepare Response
        $response = (count($results) == count(array_filter($results)))
            ? Result::success($this, "Grumphp Tasks Passed")
            : Result::error($this, "Grumphp Tasks Fail")
        ;
        $response->mergeData($results);
        if (isset($response['already-printed'])) {
            unset($response['already-printed']);
        }

        return $response;
    }
}
