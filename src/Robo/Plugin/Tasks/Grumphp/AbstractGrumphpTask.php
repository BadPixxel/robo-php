<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Robo\Php\Robo\Plugin\Tasks\Grumphp;

use Robo\Contract\BuilderAwareInterface;
use Robo\LoadAllTasks;
use Robo\Task\Base\Exec;
use Robo\Task\BaseTask;
use Symfony\Component\Console\Helper\ProgressBar;

abstract class AbstractGrumphpTask extends BaseTask implements BuilderAwareInterface
{
    use LoadAllTasks;

    /**
     * Force Grumphp Location
     *
     * @var string
     */
    private string $location = "./";

    /**
     * Enable Progress Bar
     *
     * @var bool
     */
    private bool $progress = true;

    /**
     * Set Grumphp Location Prefix
     *
     * @param null|string $location Force Path Prefix
     *
     * @return $this
     */
    public function location(?string $location): self
    {
        if ($location) {
            $this->location = $location;
        }

        return $this;
    }

    /**
     * Enable/Disable Progress Bar
     *
     * @param bool $enable
     *
     * @return $this
     */
    public function progress(bool $enable): self
    {
        $this->progress = $enable;

        return $this;
    }

    /**
     * Execute Multiple Grumphp Tasks
     *
     * @param array<string, string> $tasks
     *
     * @return array<string, ?bool>
     */
    protected function runGrumpTasks(array $tasks): array
    {
        //====================================================================//
        // Start Progress Bar
        if ($this->progress) {
            $progressBar = new ProgressBar($this->output(), count($tasks));
            $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s% %message%');
            $progressBar->setMessage("");
            $progressBar->start();
        }
        //====================================================================//
        // Executes Tasks
        $results = array();
        foreach ($tasks as $code => $name) {
            if (isset($progressBar)) {
                $progressBar->setMessage($name);
            }
            //====================================================================//
            // Execute Task
            $results[$name] = $this->runGrumpTask($code);
            //====================================================================//
            // Advance progress bar
            if (isset($progressBar)) {
                $progressBar->advance();
            }
        }
        if (isset($progressBar)) {
            $progressBar->clear();
            $this->output()->writeln("");
        }

        return $results;
    }

    /**
     * Execute A Single Grumphp Task (No Outputs)
     *
     * @param string $code Task Code
     *
     * @return null|bool
     */
    protected function runGrumpTask(string $code): ?bool
    {
        global $binary;
        //====================================================================//
        // Find Binary
        $binary = $binary ?? ($this->detectBinary() ?? false);
        if (!$binary) {
            return null;
        }
        //====================================================================//
        // Build Task Command
        $command = sprintf("php %s run --tasks=%s", $binary, $code);
        if ($this->output()->isQuiet()) {
            $command .= " -q";
        }
        //====================================================================//
        // Execute Task
        /** @var Exec $taskExec */
        $taskExec = $this->taskExec($command." -q -n");
        $result = $taskExec
            ->silent(true)
            ->setVerbosityThreshold(4)
            ->printOutput(false)
            ->run()
            ->wasSuccessful()
        ;
        //====================================================================//
        // Task Fail => Redo with Outputs
        if (!$result) {
            /** @var Exec $failTaskExec */
            $failTaskExec = $this->taskExec($command);
            $failTaskExec
                ->detectInteractive()
                ->run()
            ;
        }

        return $result;
    }

    /**
     * Identify Grumphp Binary Location
     *
     * @return null|string
     */
    private function detectBinary(): ?string
    {
        $paths = array_unique(array_filter(array(
            $_ENV["GRUMPHP_BIN"] ?? null,
            "bin/grumphp",
            "vendor/bin/grumphp",
        )));
        //====================================================================//
        // Walk on Possible Path
        foreach ($paths as $path) {
            if (is_file($this->location.$path)) {
                return $this->location.$path;
            }
        }
        $this->printTaskError('Unable to find Grumphp Binaries');
        $this->printTaskError('Binaries searched in: '.implode(", ", $paths));

        return null;
    }
}
